-- Adminer 4.8.0 PostgreSQL 13.2 (Debian 13.2-1.pgdg100+1) dump


CREATE TABLE "public"."Prece" (
    "InventāraVeids" character varying(255),
    "Ražotājs" character varying(255),
    "Modelis" character varying(255),
    "Cena" character varying(255),
    "Attēls" character varying(255)
    "Id" character varying(255)
    "Groz" character varying(255)
) WITH (oids = false);

INSERT INTO "Prece" ("InventāraVeids", "Ražotājs", "Modelis", "Cena", "Attēls", "Id", "groz") VALUES
('Boksa cimdi',	'Venum',	'Venum Gladiator 3.0 - Black/Red',	'84,98 €',	'//cdn.myshoptet.com/usr/www.fightexpert.eu/user/shop/big/10512-2_boxing-gloves-venum-gladiator-3-0-black-red.jpg?5eb20d35', '1', 'no'),
('Boksa cimdi',	'Venum',	'Venum Elite - Khaki Camo',	'84,98 €',	'//cdn.myshoptet.com/usr/www.fightexpert.eu/user/shop/big/13982_boxing-gloves-venum-elite-khaki-camo.jpg?5ec7266c', '3', 'no'),
('Boksa cimdi',	'Venum',	'Venum Gladiator 3.0 - Black/White',	'84,98 €',	'//euro.venum.com/media/catalog/product/cache/3d6373dfdbd8bf7042581a31874a0831/2/6/261ac9cf96de69caa0f9c2fee29af1919600c749_gladiator_1_1.jpg', '2', 'no');

-- 2021-05-13 17:16:16.614179+00
