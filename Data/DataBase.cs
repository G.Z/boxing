using System.Threading.Tasks;

using System.Collections.Generic;
using Npgsql;
using System.Linq;

namespace logger.Data
{
    public class DataBase
    {
        private static string connectionString =
            "User ID=postgres;Password=zobies;Host=localhost;Port=5432;Database=zobies;Connection Lifetime=0;";
        public async Task<List<Prece>> GetPrece(string userName = null)
        {  
            using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();

            var command = "select * from \"Prece\" ";
           
            
            using var cmd = new NpgsqlCommand(command, conn);
            using var reader = await cmd.ExecuteReaderAsync();
           
           
             
           
            var result = new List<Prece>();
            while(await reader.ReadAsync()){
                var s = new Prece();
                s.InventaraVeids = reader.GetString(0);
                s.Razotajs = reader.GetString(1);
                s.Modelis = reader.GetString(2);
                s.Cena = reader.GetString(3);
                s.Attels = reader.GetString(4);
                s.Id = reader.GetString(5);
                result.Add(s);
            }
            return result;
        }

        public async Task<List<Groz>> GetGroz(string userName = null)
        {  
            using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();

            var command = "select * from \"Groz\" ";
           
            
            using var cmd = new NpgsqlCommand(command, conn);
            using var reader = await cmd.ExecuteReaderAsync();
            var result = new List<Groz>();
            while(await reader.ReadAsync()){
                var s = new Groz();
                s.productId = reader.GetString(0);
                result.Add(s);
            }
            return result;
        }
    
        
        public async Task AddProductToCart(string Id) // <----- Kā padot mainigā vērtību, no cimdi.razor izsauktās
        {
           
          // var comman = "select * from \"Groz\" ";
           //var existingProductInCart = comman;
           //var alreadyExists = existingProductInCart.Any(x => x.comman == productId);  
           await using var conn = new NpgsqlConnection(connectionString);
           await conn.OpenAsync();

           string command = null;
           using var cmd = new NpgsqlCommand(null, conn);
            command = @"INSERT INTO ""Groz"" (""productId"")
                            VALUES(@Id)
                            
                            ;"; // <----  ievietojot @Id, rāda, ka id neeksistē
           cmd.CommandText = command;
           
           cmd.Parameters.AddWithValue("Id", Id);
           await cmd.ExecuteNonQueryAsync();

        }
        public async Task DeleteFromCart(string Id) // <----- Kā padot mainigā vērtību, no cimdi.razor izsauktās
        {
           
          // var comman = "select * from \"Groz\" ";
           //var existingProductInCart = comman;
           //var alreadyExists = existingProductInCart.Any(x => x.comman == productId);  
           await using var conn = new NpgsqlConnection(connectionString);
           await conn.OpenAsync();

           string command = null;
           using var cmd = new NpgsqlCommand(null, conn);
            command = @"DELETE FROM ""Groz""
                        WHERE ""productId"" = @Id
                            
                            
                            ;"; // <----  ievietojot @Id, rāda, ka id neeksistē
           cmd.CommandText = command;
           
           cmd.Parameters.AddWithValue("Id", Id);
           await cmd.ExecuteNonQueryAsync();

        }
    
    public class Prece
    {
        public string InventaraVeids { get; set; }
        public string Razotajs { get; set; }
        public string Modelis { get; set; }
        public string Cena { get; set; }
        public string Attels { get; set; }
        
        public string Id { get; set; }
        
     
    }
    
    public class Groz
    {
        public string productId {get; set; }

    }
        
}
    
}